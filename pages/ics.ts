import { add } from 'date-fns';
import { NextPageContext } from 'next';
import React from 'react';
import settings from './api/local.env';
import { events } from "./api/events";
import { formatDateRFC5545 } from './api/formatDateRFC5545';

export default class Ics extends React.Component {
  static async getInitialProps({ res, query }: NextPageContext) {
    if (!res) {
      return;
    }
    const readToken = query.readToken;
    if(typeof readToken  !== 'string'){
      return res.end('token missing');
    }
    res.setHeader("Content-Type", "text/calendar; charset=utf-8");
    res.write(getIcs(await events(readToken)));
    res.end();
  }
}

function getIcs(events: any[]): any {
  return `BEGIN:VCALENDAR
PRODID:-//No Inc//ethercalendar//EN
VERSION:2.0
CALSCALE:GREGORIAN
METHOD:PUBLISH
X-WR-TIMEZONE:Europe/Berlin
BEGIN:VTIMEZONE
TZID:Europe/Berlin
X-LIC-LOCATION:Europe/Berlin
BEGIN:DAYLIGHT
TZOFFSETFROM:+0100
TZOFFSETTO:+0200
TZNAME:CEST
DTSTART:19700329T020000
RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU
END:DAYLIGHT
BEGIN:STANDARD
TZOFFSETFROM:+0200
TZOFFSETTO:+0100
TZNAME:CET
DTSTART:19701025T030000
RRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU
END:STANDARD
END:VTIMEZONE
${events.map(event => vevent(event)).join('\n')}
END:VCALENDAR
`.replace(new RegExp('\n', 'g'), () => '\r\n');
}

function encode(str: string | null) {
  if (!str) {
    return "";
  }
  return str
    .replace(new RegExp('\\\\', 'g'), () => '\\\\')
    .replace(new RegExp(';', 'g'), () => '\\;')
    .replace(new RegExp(',', 'g'), () => '\\,')
    .replace(new RegExp('\n', 'g'), () => '\\n');
}

function vevent(event: any) {
  return `BEGIN:VEVENT
UID:${event.id}@${settings.HOST}
DTSTART:${formatDateRFC5545(event.date)}
DTEND:${formatDateRFC5545(add(event.date, { hours: 2 }))}
DTSTAMP:${formatDateRFC5545(new Date())}
DESCRIPTION:${encode(event.description)}
SUMMARY:${encode(event.title)}
ORGANIZER;CN=NONE:MAILTO:noreply@${settings.HOST}
END:VEVENT`;
}

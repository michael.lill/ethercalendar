import FullCalendar, { EventClickArg, EventInput, EventSourceInput } from '@fullcalendar/react';
import deFC from '@fullcalendar/core/locales/de';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin, { DateClickArg } from '@fullcalendar/interaction';
import Close from '@mui/icons-material/Close';
import Edit from '@mui/icons-material/Edit';
import Help from '@mui/icons-material/Help';
import RssFeed from '@mui/icons-material/RssFeed';
import EventIcon from '@mui/icons-material/Event';
import Save from '@mui/icons-material/Save';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import DateTimePicker from '@mui/lab/DateTimePicker';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import { Alert, Button, Snackbar, Stack, TextField } from '@mui/material';
import Dialog from '@mui/material/Dialog';
import { Event } from '@prisma/client';
import { addDays, addHours, compareAsc, isBefore, parseISO } from 'date-fns';
import { de } from 'date-fns/locale';
import type { NextPage } from 'next';
import Head from 'next/head';
import React, { useEffect, useState } from 'react';
import styles from '../styles/Home.module.css';
import { formatDateDe } from "./api/formatDateDe";

enum ViewType {
  List, Calendar
}

const Api = {

  Queries: {
    fetchEvents: async (readToken: string) => {
      return await fetch(`/api/event?readToken=${readToken}`)
        .then((resp) => resp.json())
        .then(data => {
          data.map((event: Event) => {
            event.date = parseISO(event.date as any)
            return event;
          })
          return data;
        });
    }
  },

  Commands: {
    createCalendar: async () => {
      return await fetch(`/api/calendar/create`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' }
      });
    },

    upsertEvent: async (event: Event, writeToken: string | undefined) => {
      return await fetch(`/api/event/upsert`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          event,
          writeToken,
        }),
      });
    },

    deleteEvent: async (event: Event, writeToken: string) => {
      return await fetch(`/api/event/delete`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          event,
          writeToken
        }),
      });
    }
  }

}

const getQueryParam = (param: string) => {
  if (typeof window === 'undefined') {
    return undefined;
  }
  const urlSearchParams = new URLSearchParams(window.location.search);
  const params = Object.fromEntries(urlSearchParams.entries());
  return params[param];
}

const Home: NextPage = () => {
  const [events, setEvents] = useState([] as Event[]);
  const [snackBarOpen, setSnackBarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState("");
  const readToken = getQueryParam('readToken');
  const writeToken = getQueryParam('writeToken');


  useEffect(() => {
    if (typeof readToken === 'string') {
      Api.Queries.fetchEvents(readToken).then(data => setEvents(data));
    } else {
      Api.Commands.createCalendar().then(res => {
        res.json().then(data => {
          if (typeof data === 'object') {
            location.href = `/?readToken=${data.readToken}&writeToken=${data.writeToken}`;
          }
        });
      })
    }
  }, []);

  const [selectedEvent, setSelectedEvent] = useState(undefined as Event | undefined);
  const [view, setView] = useState(ViewType.Calendar)

  const onDateClick = (args: DateClickArg) => {
    if (isBefore(args.date, addDays(new Date(), -1))) {
      return showSnack("Datum liegt in der Vergangenheit");
    }
    if (typeof getQueryParam('writeToken') !== 'string') {
      return showSnack("Fehlende Schreibrechte.");
    }
    setSelectedEvent({
      id: -1,
      description: '',
      date: addHours(args.date, 18),
    } as Event);
  }

  const onEventClick = (args: EventClickArg) => {
    setSelectedEvent(args.event.extendedProps as Event);
  }

  const showSnack = (message: string) => {
    setSnackbarMessage(message);
    setSnackBarOpen(true);
  }

  function tranform(events: Event[]): EventSourceInput {
    return events.map(event => {
      return {
        allDay: false,
        start: event.date.toISOString(),
        backgroundColor: '#2ab7ca',
        title: event.title ?? '',
        extendedProps: event,
      } as EventInput;
    });
  }

  if (typeof readToken !== 'string') {
    return <>
      <div>loading...</div></>;
  }

  return (
    <>
      <Head>
        <title>ethercalendar</title>
        <meta name="description" content="ethercalendar - like etherpad but calendar" />
        <link rel="icon" href="/favicon.ico" />
        {typeof readToken === 'string' &&
          <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href={`/feed?readToken=${readToken}`} />
        }
      </Head>
      <div className={styles.container}>
        <Snackbar
          open={snackBarOpen}
          autoHideDuration={5000}
          onClose={() => setSnackBarOpen(false)}
          anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        >
          <Alert severity="info">
            {snackbarMessage}
          </Alert>
        </Snackbar>

        <header style={{ display: 'flex', justifyContent: 'space-between', flexWrap: 'wrap', gap: '1em' }} className="mb-5">
          <div>Zeitzone: {Intl.DateTimeFormat().resolvedOptions().timeZone}</div>

          <div>
            {view === ViewType.Calendar &&
              <Button variant="contained" onClick={() => setView(ViewType.List)}>
                Listenansicht
              </Button>
            }
            {view === ViewType.List &&
              <Button variant="contained" onClick={() => setView(ViewType.Calendar)}>
                Kalenderansicht
              </Button>
            }
            {typeof readToken === 'string' &&
              <>
                <a href={`/feed?readToken=${readToken}`}>
                  <Button variant="contained" style={{ marginLeft: '5px' }} color="success" aria-label="close">
                    <RssFeed />
                  </Button>
                </a>
                <a href={`/ics?readToken=${readToken}`}>
                  <Button variant="contained" style={{ marginLeft: '5px' }} color="success" aria-label="close">
                    <EventIcon />
                  </Button>
                </a>
              </>
            }
            <Button variant="contained" style={{ marginLeft: '5px' }} onClick={() => {
              window.alert('NYI');
            }} color="success" aria-label="close">
              <Help />
            </Button>
          </div>
        </header>
        <main className={styles.main}>

          {view === ViewType.Calendar &&
            <FullCalendar
              plugins={[dayGridPlugin, interactionPlugin]}
              initialView={"dayGridMonth"}
              height="100%"
              weekNumbers
              firstDay={1}
              locale={deFC}
              fixedWeekCount={false}
              selectable
              eventTimeFormat={{
                hour: '2-digit',
                minute: '2-digit',
                hour12: false
              }}
              eventClick={onEventClick}
              dateClick={onDateClick}
              events={tranform(events)}
            />
          }
          {view === ViewType.List &&
            <Stack spacing={2}>
              {events.sort((a, b) => {
                return compareAsc(a.date, b.date)
              }).map((event, idx) => <div key={idx} onClick={() => setSelectedEvent(event)} style={{ cursor: 'pointer', display: 'grid', gridGap: '0.5em', gridTemplateColumns: 'repeat(auto-fill, minmax(200px, 1fr))', border: '2px outset rgba(0,0,0,0.3)', padding: '0.25em' }}>
                <EventSummary event={event} />
              </div>)}
            </Stack>
          }

          {selectedEvent &&
            <EventDialog setSelectedEvent={setSelectedEvent} selectedEvent={selectedEvent} fetchEvents={async () => {
              if (typeof readToken === 'string') {
                setEvents(await Api.Queries.fetchEvents(readToken));
              }
            }} showSnack={showSnack} />
          }

        </main>
        <footer style={{ display: 'flex', justifyContent: 'center', margin: '1em 0', wordBreak: 'break-all', textAlign: 'center' }}>
          <Stack spacing={2}>
            {readToken &&
              <a href={`${location.protocol}//${location.host}/?readToken=${readToken}`} style={{ textDecoration: 'underline' }}>
                <b>Lesezugriff</b> ({`${location.protocol}//${location.host}/?readToken=${readToken}`})
              </a>
            }
            {writeToken &&
              <a href={`${location.protocol}//${location.host}/?readToken=${readToken}&writeToken=${writeToken}`} style={{ textDecoration: 'underline' }}>
                <b>Schreibzugriff</b> ({`${location.protocol}//${location.host}/?readToken=${readToken}&writeToken=${writeToken}`})
              </a>
            }
          </Stack>
        </footer>
      </div>
    </>
  )
}

function EventSummary({ event }: { event: any }) {
  return <Stack spacing={2}>
    <div>{formatDateDe(event.date)}</div>
    <div>{event.title}</div>
    <div>{event.description}</div>
  </Stack>
}

function EventView({ selectedEvent, setSelectedEvent, fetchEvents, showSnack }: { selectedEvent: Event, setSelectedEvent: Function, fetchEvents: Function, showSnack: Function }) {
  return <>
    <div className="mb-5">
      <EventSummary event={selectedEvent} />
    </div>
  </>
}

function EventEditor({ event, setEvent }: { event: Event, setEvent: Function }) {
  return <Stack spacing={2}>
    <div>
      <LocalizationProvider dateAdapter={AdapterDateFns} locale={de}>
        <DateTimePicker disablePast minutesStep={5} label="Wann" value={event.date} onChange={(newValue) => {
          if (!newValue) {
            return;
          }
          setEvent({
            ...event,
            date: newValue
          });
        }} renderInput={(props) => <TextField {...props} />} />
      </LocalizationProvider>
    </div>
    <div>
      <TextField id="event-title" fullWidth required defaultValue={event.title} label="Titel" variant="standard" />
    </div>
    <div>
      <TextField id="event-description" fullWidth required multiline minRows={3} defaultValue={event.description} label="Beschreibung" variant="standard" />
    </div>
  </Stack>;
}

function EventDialog({ selectedEvent, setSelectedEvent, fetchEvents, showSnack }: { selectedEvent: Event, setSelectedEvent: Function, fetchEvents: Function, showSnack: Function }) {

  const [editMode, setEditMode] = useState(selectedEvent.id === -1);

  return <Dialog open maxWidth="sm" fullWidth>

    <div style={{ padding: '1em' }}>
      {editMode &&
        <h2>Termin anlegen/editieren</h2>
      }
      <div className="mb-5" style={{ display: 'flex', justifyContent: 'space-between' }}>
        <div>
          {!editMode && typeof getQueryParam('writeToken') === 'string' &&
            <Button variant="contained" onClick={() => setEditMode(!editMode)} aria-label="close">
              <Edit />
            </Button>
          }
        </div>
        <Button variant="contained" onClick={() => setSelectedEvent(undefined)} aria-label="close">
          <Close />
        </Button>
      </div>
      {editMode &&
        <EventEditor event={selectedEvent} setEvent={setSelectedEvent} />
      }
      {!editMode &&
        <EventView setSelectedEvent={setSelectedEvent} selectedEvent={selectedEvent} fetchEvents={fetchEvents} showSnack={showSnack} />
      }
      {editMode &&
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div>
          </div>
          {typeof getQueryParam('writeToken') === 'string' &&
            <Button variant="contained" onClick={() => {
              const title = (document.querySelector('#event-title') as any)?.value ?? '';
              const description = (document.querySelector('#event-description') as any)?.value ?? '';
              if (title.length < 1) {
                return showSnack("Titel fehlt?")
              } if (description.length < 1) {
                return showSnack("Beschreibung fehlt?")
              }
              setSelectedEvent(undefined);
              Api.Commands.upsertEvent({
                ...selectedEvent,
                title,
                description
              }, getQueryParam('writeToken'))
                .then(() => fetchEvents());

            }} color="success" aria-label="close">

              <Save />
            </Button>
          }
        </div>
      }
    </div>
  </Dialog >;
}

export default Home
import { NextPageContext } from 'next';
import React from 'react';
import { events } from "./api/events";
import { formatDateDe } from "./api/formatDateDe";
import settings from './api/local.env';
import {Event} from '@prisma/client';

export default class Rss extends React.Component {
  static async getInitialProps({ res, req, query }: NextPageContext) {
    if (!res || !req) {
      return;
    }
    const readToken = query.readToken;
    if(typeof readToken  !== 'string'){
      return res.end('token missing');
    }
    res.setHeader("Content-Type", "text/xml");
    res.write(getRssXml(await events(readToken)));
    res.end();
  }
}

// NYI
function getRssXml(events: any[]) {
  return `<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <id>urn:uuid:47a198d8-3bd2-49ac-9c90-57b776e0fdae</id>
    <title type="text">ethercalendar</title>
    <subtitle type="html">ethercalendar - like etherpad but calendar</subtitle>
    <updated>${new Date().toISOString()}</updated>
    ${events.map(r => event(r)).join('\n')}
</feed>`
}

// NYI
function event(event: Event) {
  return `<entry>
  <title type="html"><![CDATA[${formatDateDe(event.date)} ${htmlEncode(event.title)}]]></title>
  <author><name>none</name></author>
  <id>http://${settings.HOST}/?eventId=${event.id}/</id>
  <updated>${new Date().toISOString()}</updated>
  <published>${new Date().toISOString()}</published>
  <summary type="html"><![CDATA[${formatDateDe(event.date)} ${htmlEncode(event.description)}]]></summary>
</entry>`;
}

function htmlEncode(str: string|null) {
  if(!str){
    return '';
  }
  return str
    .replace(/[\u00A0-\u9999<>\&]/gim, (i) => '&#' + i.charCodeAt(0) + ';')
    .replace(new RegExp('\n', 'g'), () => '<br />');
}
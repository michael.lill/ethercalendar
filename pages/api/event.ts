import type { NextApiRequest, NextApiResponse } from 'next';
import { events } from "./events";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<any>
) {
  const readToken = req.query.readToken || req.body.readToken;
  const allEvents = await events(readToken);

  res.status(200).json(allEvents)
}

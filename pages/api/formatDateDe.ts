import { format } from "date-fns";
import { de } from "date-fns/locale";

export function formatDateDe(date: Date) {
  return format(date, 'dd.MM.yyyy HH:mm', {
    locale: de
  });
}

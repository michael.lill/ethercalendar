import { NextApiRequest, NextApiResponse } from 'next';
import { v4 } from 'uuid';
import { prismaClient } from "../prisma";

export default async function handle(req: NextApiRequest,
  res: NextApiResponse<any>) {

  const calendar = await createCalendar();

  res.json({
    readToken: calendar.readToken,
    writeToken: calendar.writeToken
  })
}

async function createCalendar() {
  return (await prismaClient().calendar.create({
    data: {
      readToken: v4(),
      writeToken: v4()
    }
  }));
}

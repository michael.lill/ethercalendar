import { prismaClient } from "./prisma";


export async function events(readToken: string) {
  if(readToken.length <= 3){
    return [];
  }
  return await prismaClient().event.findMany({
    where: {
      calendar: {
        readToken,
      }
    },
    select: {
      id: true,
      date: true,
      title: true,
      description: true
    }
  });
}

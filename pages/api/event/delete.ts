import { Event } from '@prisma/client';
import { NextApiRequest, NextApiResponse } from 'next';
import { prismaClient } from "../prisma";

export default async function handle(req: NextApiRequest,
  res: NextApiResponse<Event>) {
  const eventId = req.body.event.id;
  const writeToken = req.body.writeToken;

  const event = await prismaClient().event.findUnique({
    where: {
      id: eventId
    },
    include: {
      calendar: true
    }
  });

  if (event?.calendar.writeToken !== writeToken) {
    return res.status(403);
  }

  const result = await prismaClient().event.delete({
    where: {
      id: event?.id
    }
  })
  res.json(result)
}
import { Event } from '@prisma/client';
import { NextApiRequest, NextApiResponse } from 'next';
import { v4 } from 'uuid';
import { prismaClient } from "../prisma";

export default async function handle(req: NextApiRequest,
  res: NextApiResponse<any>) {
  const event = req.body.event as Event;
  const writeToken = req.body.writeToken;
  delete (event as any).calendarId;
  const eventId = event.id;

  if (!(typeof writeToken === 'string' && writeToken.length > 3)) {
    return res.status(403);
  }
  const calendarId = await findCalendarId(writeToken);
  if (typeof calendarId !== 'number') {
    return res.status(403);
  }

  // update event
  if (eventId !== -1) {
    await prismaClient().event.update({
      where: {
        id: eventId,
      },
      data: {
        ...event,
        id: undefined,
        calendarId,
        updatedAt: new Date()
      } as any,
    })
    res.json(undefined)
  }

  // create event in existing calendar
  else {
    await prismaClient().event.create({
      data: {
        ...event,
        id: undefined,
        calendarId
      } as any,
    })
    res.json(undefined)
  }
}

async function findCalendarId(writeToken: any) {
  return (await prismaClient().calendar.findUnique({
    where: {
      writeToken
    },
    select: {
      id: true
    }
  }))?.id;
}


#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

while [[ "$#" -gt 0 ]]; do
    case $1 in
        -t|--token) token="$2"; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

if [ -z ${token+x} ];
    then echo "-t|--token is unset";
    exit 1;
fi

set -euo pipefail

mkdir -p /tmp/ical-data

echo "Token: $token"

curl http://localhost:3000/ics?readToken=$token > /tmp/ical-data/calendar.ics

docker run --volume=/tmp/ical-data:/data faph/icalendar-validator /data/calendar.ics

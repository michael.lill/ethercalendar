-- CreateTable
CREATE TABLE "Calendar" (
    "id" SERIAL NOT NULL,
    "readToken" TEXT NOT NULL,
    "writeToken" TEXT NOT NULL,

    CONSTRAINT "Calendar_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Event" (
    "id" SERIAL NOT NULL,
    "date" TIMESTAMP(3) NOT NULL,
    "title" TEXT NOT NULL,
    "description" TEXT,
    "calendarId" INTEGER NOT NULL,

    CONSTRAINT "Event_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Calendar_readToken_key" ON "Calendar"("readToken");

-- CreateIndex
CREATE UNIQUE INDEX "Calendar_writeToken_key" ON "Calendar"("writeToken");

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_calendarId_fkey" FOREIGN KEY ("calendarId") REFERENCES "Calendar"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

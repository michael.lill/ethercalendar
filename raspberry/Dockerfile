FROM node:lts-slim

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN addgroup --gid 1001 --quiet nextjs
RUN adduser --disabled-password --gecos "" --gid 1001 nextjs
RUN chown -R nextjs:nextjs /usr/src/app

ENV NODE_ENV production

COPY --chown=nextjs:nextjs public ./public
COPY --chown=nextjs:nextjs .next ./.next
COPY --chown=nextjs:nextjs package.json ./package.json
COPY --chown=nextjs:nextjs package-lock.json ./package-lock.json
COPY --chown=nextjs:nextjs schema.prisma ./schema.prisma
COPY --chown=nextjs:nextjs migrations ./migrations

RUN apt-get update
RUN apt-get -y install openssl wget

ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

USER nextjs:nextjs

RUN wget --quiet https://github.com/pantharshit00/prisma-rpi-builds/releases/download/3.2.1/query-engine
RUN wget --quiet https://github.com/pantharshit00/prisma-rpi-builds/releases/download/3.2.1/libquery_engine.so
RUN wget --quiet https://github.com/pantharshit00/prisma-rpi-builds/releases/download/3.2.1/introspection-engine
RUN wget --quiet https://github.com/pantharshit00/prisma-rpi-builds/releases/download/3.2.1/migration-engine
RUN wget --quiet https://github.com/pantharshit00/prisma-rpi-builds/releases/download/3.2.1/prisma-fmt

RUN chmod +x query-engine
RUN chmod +x introspection-engine
RUN chmod +x migration-engine
RUN chmod +x prisma-fmt
RUN mv libquery_engine.so libquery_engine-linux-arm-openssl-1.1.x.so.node

ENV PRISMA_QUERY_ENGINE_BINARY /usr/src/app/query-engine
ENV PRISMA_QUERY_ENGINE_LIBRARY /usr/src/app/libquery_engine-linux-arm-openssl-1.1.x.so.node
ENV PRISMA_INTROSPECTION_ENGINE_BINARY /usr/src/app/introspection-engine
ENV PRISMA_MIGRATION_ENGINE_BINARY /usr/src/app/migration-engine
ENV PRISMA_FMT_BINARY /usr/src/app/prisma-fmt
ENV PRISMA_CLI_QUERY_ENGINE_TYPE binary
ENV PRISMA_QUERY_ENGINE_TYPE binary

RUN npm install
RUN npm run generate

ENV NEXT_TELEMETRY_DISABLED 1

EXPOSE 3000

CMD npx prisma migrate deploy ; npm run start
